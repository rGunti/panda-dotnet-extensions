# Welcome to Panda .NET Extensions
**Panda .NET Extensions** are a set of packages for your .NET project
containing various different extensions and base implementations
of all kind. Most of these packages probably fall in the category
["boilerplate code"](https://en.wikipedia.org/wiki/Boilerplate_code) or 
["syntactic sugar"](https://en.wikipedia.org/wiki/Syntactic_sugar)
so none are considered a necessity but more of a nice-to-have.

> This library is currently still in the initial build-up phase.
> As such, please stand by for more updates soon

## Package Source
You can add the GitLab NuGet source to your project or environment
to install these libraries:

**URL**
```
https://gitlab.com/api/v4/projects/28847354/packages/nuget/index.json 
```

**Command Line**
```
dotnet nuget add source \
  "https://gitlab.com/api/v4/projects/28847354/packages/nuget/index.json" \
  --name panda-dotnet
```
