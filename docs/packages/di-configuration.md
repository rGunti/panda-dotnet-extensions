# DI.Configuration
This package provides various configuration related extensions.

## Connection Strings
### `IConfiguration.GetConnectionString()`
`GetConnectionString()` is a built-in extension method provided by the `Microsoft.Extensions.Configuration` package.
Panda adds an overload with an additional parameter to strip a URI scheme from the connection string.

#### Example
Provided the following configuration:
```json
{
  "ConnectionStrings": {
    "Default": "file:///var/log/app.db"
  }
}
```

```c#
config.GetConnectionString("Default", true);
// -> "/var/log/app.db"

config.GetConnectionString("Default", false);
// -> "file:///var/log/app.db"
```

### `string.GetScheme()`
Returns the (first) scheme of the given URI.

```c#
"file:///var/log/app.db".GetScheme();
// -> "file"

"http://file:///tmp/file".GetScheme();
// -> "http"
```

### `string.StripScheme()`
Returns a new string with the first scheme in the provided URI removed.
If the string contains multiple URI schemes, only the first will be stripped.

```c#
"http://example.com/some/route".StripScheme();
// -> "example.com/some/route"

"file://http://example.com".StripScheme();
// -> "http://example.com"
```

## Config Objects
### `IServiceCollection.AddConfigObject<T>()`
`AddConfigObject<T>()` adds a singleton instance of `T` to the service collection.
This requires an `IConfiguration` instance to be present in the `IServiceProvider`
once in use.

Internally, this extension method uses the Microsoft-provided `Binder` package to
bind the configuration section to a class. This means that in case the section is
not present, the injection might fail and thus cause problems during runtime. 

#### Example
```json
{
  "ClientConfig": {
    "ServerAddress": "http://example.com/my-server",
    "UserId": 123456
  }
}
```

```c#
// = ClientConfig.cs
// First, we create a class to hold our configuration values
class ClientConfig {
  public string ServerAddress { get; set; }
  public int UserId { get; set; }
}

// = Startup.cs
// On application startup, we register our class as a configuration object
// and map it to the configuration section "ClientConfig".
services.AddConfigObject<ClientConfig>("ClientConfig");

// = SomeService.cs
// Finally, we use the configuration class somewhere in our application
// using DI.
class SomeService {
  private readonly ClientConfig _clientConfig;
  // ...

  public SomeService(ClientConfig clientConfig, /* ... */) {
    _clientConfig = clientConfig;
    // ...
  }

  public void ConnectToServer() {
    // We connect (or do something) with the configuration values that we
    // were provided with by the ClientConfig section.
    _magicServer.Connect(_clientConfig.ServerAddress, _clientConfig.UserId);
    /*
    _clientConfig.ServerAddress == "http://example.com/my-server"
    _clientConfig.UserId == 123456
     */
  }
}
```
