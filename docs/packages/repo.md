# Repository
The `Repository` packages provide a basic implementation of the repository pattern
for various storage options. The repository pattern aims to decouple the "database
logic" from the application logic. This usually means that objects stored in the
database are different then the once used at runtime. A possible distinction could
be:

- Entity Objects ("EOs") are stored in the database
- Data Transfer Objects ("DTOs") are constructed from EOs and used in the application
  logic or exposed via i.e. an API

## Supported Data Sources

Currently supported data sources are:

- [Memory](repository-drivers/memory.md) (`using ConcurrentDictionary`)
- [Entity Framework Core](repository-drivers/entity-framework.md)
- [LiteDB](repository-drivers/litedb.md) (in-memory document store)
- [MongoDB](repository-drivers/mongodb.md)

These data source implementations are called "Drivers" and are stored in their own
packages.

## Core Interfaces
### `IEntity<TKey>`
At the core of all Repository packages lies the `IEntity` interface. The `IEntity`
interface prescribes that an object must have a primary key property which's type
is prescribed by the `TKey` generic type argument.

### `IRepository<TEntity, TKey>`
The `IRepository` interface acts as access to the entities stored in the underlying
storage medium.  For most databases, this would be equivalent to the table.

The `IRepository` interface prescribes the following operations to be available:

- `GetById(TKey)`: Returns one object with the given ID or null if it doesn't
  exist.
- `ExistsWithId(TKey)`: Returns `true`, if an object with the given ID exists.
- `[TKey]`: Same as `GetById(TKey)` but throws an `ArgumentOutOfRangeException`
  if the object doesn't exist.
- `Insert(TEntity)`: Stores a new object in the data source.
- `Update(TEntity)`: Updates a given object in the data source. The ID is pulled
  from the object itself.
- `Delete(TKey)`: Removes an object with the given ID from the data source.
- `Delete(TEntity)`: Same as `Delete(TKey)` but you can pass the object itself.
- `All`: Exposes an `IEnumerable<TEntity>` to do basic queries directly using
  LINQ. Note that this is heavily dependant on the underlying storage technology
  and might cause the whole data source to be loaded and evaluated in-memory.
  Therefore, it is recommended to implement queries by adding a method to
  your a custom interface and implement the query in the database's "native"
  library.

## On the topic of Statefulness and Tracking
Libraries like Entity Framework track their objects once they are entered. This
usually means that objects can be loaded from the source and modified. In
Entity Framework, changes made to the object can then be returned by simply calling
`DbContext.SaveChanges()`.

However this feature is not desirable in this implementation of the Repository
pattern. What this essentially means is that to update an object, you have to
specifically call the `.Update()` function of your repository to invoke such
an action.
