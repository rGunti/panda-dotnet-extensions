# Entity Framework Core
[Entity Framework Core](https://docs.microsoft.com/en-us/ef/core/) is an ORM
developed by Microsoft. It supports many different relational databases, like
MS-SQL, MySQL, SQLite, Oracle, Postgres, etc.

## DI Registration
The package doesn't currently provide any method of adding EF Core support.
Please refer to the EF Core documentation on how to register your dependencies.
You will need to have a custom `DbContext` available in your service collection to
make use of the repository implementation.

For an example, refer to the EF Unit Tests in the project
(`PandaDotNet.Tests.Repo.Drivers.EntityFramework`).
