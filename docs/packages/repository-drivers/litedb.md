# LiteDB
[LiteDB](https://www.litedb.org/) is an open source MongoDB-like database
with zero configuration. It stores its data in a single datafile (like SQLite).

## DI Registration
To add LiteDB support to your DI container, use `.AddLiteDbDriver(connectionString)`
on your service collection. This will add a `ILiteDatabase` implementation as a
singleton to your container.

```c#
services.AddLiteDbDriver("./my-data.db");

// or if you need your DB file to be used by multiple processes:
services.AddLiteDbDriver("Filename=./my-data.db;Connection=shared");
```

For how to describe your connection in a connection string, refer to the LiteDB
documentation: [Connection String - LiteDB](https://www.litedb.org/docs/connection-string/).
