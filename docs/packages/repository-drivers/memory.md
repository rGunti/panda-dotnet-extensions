# Memory
The Memory driver uses the `ConcurrentDictionary` as its storage making it thread-safe to use.
However this implementation is only intended for use in cases where persistency is not required,
like Unit Tests for example.

You have two base implementation at your disposal if you want to use the Memory implementation:

## `MemoryRepository`
This repository simply provides an interface to the `ConcurrentDictionary` and nothing more.

## `StringIdGeneratingMemoryRepository`
This repository is a special implementation which only accept `string`s as primary keys.
However it automatically generates IDs when a new object is inserted, using
`Guid.NewGuid()` as the generator. If you want to implement your own ID generator,
simply override the `GenerateNewId()` method.
