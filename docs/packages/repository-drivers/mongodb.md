# MongoDB
[MongoDB](https://www.mongodb.com/) is a NoSQL, document storage database based
on JSON documents. It's advantage is the possiblitly of storing arbitrary documents
in one collection without the need for describing the schema first.

**Note**: If you want to use a MongoDB-like storage solution locally (stored in a file
rather than a server) check out [LiteDB](litedb.md).

## DI Registration
To add MongoDB support to your DI container, use `.AddMongoDbDriver(connectionString)`
on your service collection. This will add a `IMongoClient` implementation as a
singleton to your container and also configure a few other things for you.

```c#
services.AddMongoDbDriver("mongodb://localhost:27017/MyTestDatabase");
```

### What else is provided?
The registration also adds the following objects to the service collection:

- `MongoUrl`: This is the URL that is used to connect to your MongoDB server.
  It is used for constructing any other MongoDB dependencies.
- `IMongoClient`: This is a client to interact with the database server.
- `IMongoDatabase`: This is a quick reference to the database mentioned in
  your connection string. In the example above, the database would be
  `"MyTestDatabase"`.

Note that all dependencies will be registered as Singletons. If you want to change
the service lifetime, you will need to register the dependencies yourself. Refer
to the libraries source code (`MongoDbExtensions.cs`) for more information.

In addition, `.AddMongoDbDriver()` adds the `StringObjectIdGenerator` as an
ID generator for the type `string` to Mongo's `BsonSerializer`. This ensures
that you can use simple string properties in your Entity objects instead of
having to use Mongo's `ObjectId` while still getting an automatically generated
ID for new documents.
