# Time
This package contains a few extension methods to quickly create
`TimeSpan` objects and do arithmetics with time. Most of these
extension methods are more aimed at writing Unit Test code where
hard-coded values are usually used more often then in logic code.

## Time Unit extensions
The following extensions are applied to `int` and `double`.

- `.Milliseconds()` / `.Millisecond()`
- `.Seconds()` / `.Second()`
- `.Minutes()` / `.Minute()`
- `.Hours()` / `.Hour()`
- `.Days()` / `.Day()`

```c#
25.Milliseconds() // -> equivalent to TimeSpan.FromMilliseconds(25)
```

Since .NET allows arithmetics with TimeSpan objects, you can
simply add or subtract values produced by these extension methods:
```c#
1.Hour() - 15.Minutes() // -> == 45.Minutes() == TimeSpan.FromMinutes(45)
```
