# Utils
This package provides an unsorted set of random utilities that
either don't warrant their own package or are simply useful in various
situations.

## `.Or()`
The `.Or()` extension checks the subject of the call for `null`. If it is `null`,
the alternatively provided object is returned.

There is also a lazily constructed alternative available, allowing you to construct
the alternative value in a lambda function on demand instead of providing it upfront.

```c#
var john = new { Name = "John" };
var jane = new { Name = "Jane" };

john.Or(jane); // -> returns john

john = null;
john.Or(jane); // -> returns jane

john.Or(() => new { Name = "Marry" });
// -> constructs and returns a new object with Name = "Marry"
jane.Or(() => new { Name = "Marry" });
// -> returns jane, the anonymous object with Name = "Marry" is never constructed
```

## `.OrThrow()`
This works similar to `.Or()` but instead of returning an alternative object,
it throws an Exception. This is useful for i.e. validation where the object
may not be null.

The exception to be thrown is determined in a lambda function.

```c#
var john = new { Name = "John" };
var noone = null;

john.OrThrow(() => new Exception("You can't be null!"));
// -> this just returns john

noone.OrThrow(() => new Exception("You can't be null!"));
// -> this will throw an exception
```

You can, of course, also describe the Exception to be constructed in an
externally defined method.

```c#
public class UserRequiredException : Exception {
    // ...
}

public Exception CreateUserRequiredException() {
  return new UserRequiredException("User cannot be empty!");
}

User john = new User("john", "john.doe@example.com");
User jane = null;

john.OrThrow(CreateUserRequiredException);
// -> this will just return john
jane.OrThrow(CreateUserRequiredException);
// -> this will throw the custom exception
```

> **Where this has been used**:
> In the [Repository](repo.md) packages, `.OrThrow()` has been used
> to validate that an object with a given ID exists in the underlying
> database when requested using the Indexer. See the excerpt below from
> the LiteDB implementation:
>
> ```c#
> /// <inheritdoc />
> public virtual TEntity this[TKey id]
>   => GetById(id).OrThrow(() => new ArgumentOutOfRangeException(
>       nameof(id),
>       $"No entity of type {typeof(TEntity)} with ID {id} exists"));
> ```
