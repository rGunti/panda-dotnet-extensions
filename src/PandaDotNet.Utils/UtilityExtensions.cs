﻿using System;

namespace PandaDotNet.Utils
{
    /// <summary>
    /// An unordered set of various utilities
    /// </summary>
    public static class UtilityExtensions
    {
        /// <summary>
        /// Returns the provided object unless it is null.
        /// If the provided object is null, the alternative
        /// object will be returned.
        /// </summary>
        /// <param name="obj">Object to be tested</param>
        /// <param name="alternative">
        /// The alternative object to be returned in case
        /// <see cref="obj"/> is null.
        /// </param>
        /// <seealso cref="Or{T}(T,Func{T})"/>
        public static T Or<T>(this T obj, T alternative)
        {
            return obj == null ? alternative : obj;
        }

        /// <summary>
        /// Returns the provided object unless it is null.
        /// If the provided object is null, an alternative
        /// object will be constructed using the provided
        /// factory method and returned.
        /// </summary>
        /// <param name="obj">Object to be tested</param>
        /// <param name="alternativeFactory">
        /// A factory function that constructs the alternative
        /// value if <see cref="obj"/> is null.
        /// </param>
        /// <seealso cref="Or{T}(T,T)"/>
        public static T Or<T>(this T obj, Func<T> alternativeFactory)
        {
            return obj == null ? alternativeFactory() : obj;
        }

        /// <summary>
        /// Returns the object as is unless it is null.
        /// If the object is null, a custom exception will
        /// be thrown which is constructed using a provided
        /// <see cref="exceptionFactory"/>.
        /// </summary>
        /// <param name="obj">The object to be tested</param>
        /// <param name="exceptionFactory">
        /// A factory method for the exception to be thrown
        /// in case <see cref="obj"/> is null. The method provides
        /// no arguments and must return an <see cref="Exception"/>.
        /// </param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static T OrThrow<T>(this T obj, Func<Exception> exceptionFactory)
        {
            if (obj == null)
            {
                throw exceptionFactory();
            }
            return obj;
        }

        /// <summary>
        /// A specific implementation of <see cref="OrThrow{T}"/>
        /// that throws an <see cref="ArgumentNullException"/> if the
        /// provided object is null.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="paramName"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T OrThrowNullArg<T>(this T obj, string paramName)
        {
            return obj.OrThrow(() => new ArgumentNullException(paramName));
        }
    }
}